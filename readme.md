HYDE
====

Tested with Hyde version [0.8.7](https://github.com/hyde/hyde/tree/v0.8.7).


INSTALL
=======

Copy into hyde directory

ENABLE
======

plugins:
    - hyde.ext.plugins.meta.MetaPlugin
    - ext.runreplace.RunReplacePlugin


USAGE
=====

Just use it in a html file.

    ::-runreplace

    # -*- coding: utf-8 -*-

    if __name__ == "__main__":
        print "This will appear in the html page instead of the runreplace block"

    -::
