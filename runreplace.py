# -*- coding: utf-8 -*-
"""
Runs python code and replaces the text block with it
"""
import hyde.plugin
import re
import os
import tempfile
import subprocess
import codecs

class  RunReplacePlugin(hyde.plugin.Plugin):
    def __init__(self, site):
        super(RunReplacePlugin, self).__init__(site)
        self._findBlockRE =  re.compile(
                r"^::-runreplace[^\n]*?\n(.*?)\n-::", 
                re.MULTILINE|re.UNICODE|re.DOTALL)
    @property
    def plugin_name(self):
        """
        The name of the plugin. Makes an intelligent guess.
        """
        return self.__class__.__name__.replace('Plugin', '').lower()

    def replRun(self, match):
        ret = "Would RUN and REPLACE:  \n{% syntax %} "+match.group(1)+" {% endsyntax %}"
        tmpfile=None
        try:
            try:
                workdir = tempfile.gettempdir()
                fd, tmpfile= tempfile.mkstemp(".py", "runreplace", workdir, True)    
                pyf = codecs.open(tmpfile, "w", encoding="utf-8")
            except Exception as e:
                self.logger.error("\t\tTemp file generation error "+str( e))
            if pyf:
                pyf.write(match.group(1).decode("utf-8"))
                pyf.close()
                self.logger.debug( "\texecuting: "+str(tmpfile))
                p = subprocess.Popen("python %s"%( tmpfile), stdout=subprocess.PIPE, shell=True)
                try:
                    ret = p.communicate()[0].decode("UTF-8")
                except UnicodeDecodeError as ue:
                    self.logger.error( "\t\tUnicode error during communication"+str( ue))
            else:
                self.logger.error("Could not open tempfile for writing"+str( tmpfile))
        except Exception as e:
            self.logger.error(str(type(e)))
            self.logger.error("\tException replRun -- "+str(e))
        finally:
            try:
                if os.path.exists(tmpfile):
                    #os.remove(tmpfile)
                    pass
            except Exception as e:
                self.logger.error("During final: "+str(e))
        return ret.encode("UTF-8")

    def begin_text_resource(self, resource, text):
        if not resource.source_file.kind == "html":
            return text
        self.logger.debug("RunReplace is running on"+str(resource))
        # store current dir
        here = os.getcwd()
        #temporarily switch to dir of source file for relative paths
        os.chdir(os.path.dirname( resource.path))
        try:
            text = self._findBlockRE.sub(self.replRun, text.encode("UTF-8"))
        except Exception as e:
            self.logger.error(str(type(e)))
            self.logger.error("\tException in begin_text_resource"+str(e))
        #change back to current dir
        os.chdir(here) 
        return text.decode("UTF-8")


